﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Урок_1
{
    public partial class Calculator : Form
    {
        int odd_amount = 0;
        public Calculator()
        {
            InitializeComponent();
            dataGridView1.ColumnCount = 1;
            dataGridView1.RowCount = 1;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            label1.Text = $"Кількість парних елементів = {odd_amount}";
        }
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            dataGridView1.ColumnCount = (int)numericUpDown1.Value;
        }
        private void button1_Click_1(object sender, EventArgs e) // Відкриває файл
        {
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|dat files (*.dat)|*.dat";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (openFileDialog1.FilterIndex == 1) // Якщо txt 
                {
                    FileOps.FindOddText(openFileDialog1.FileName, dataGridView1);
                }

                else if (openFileDialog1.FilterIndex == 2) // Якщо dat
                {
                    FileOps.FindOddData(openFileDialog1.FileName,dataGridView1);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e) // Зберігає файл 
        {
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|dat files (*.dat)|*.dat";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (saveFileDialog1.FilterIndex == 1)
                {
                    FileOps.CreateTextFile(saveFileDialog1.FileName, dataGridView1);
                }

                else if (saveFileDialog1.FilterIndex == 2)
                {
                    FileOps.CreateDataFile(saveFileDialog1.FileName, dataGridView1);
                }
            }

        }
    }
    }