﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Урок_1
{
    internal class FileOps
    {
        public static void CreateTextFile(string fileName, DataGridView grid)
        {
            StreamWriter sw = new StreamWriter(fileName);
            for (int i = 0; i < grid.ColumnCount; i++)
            {
                sw.WriteLine(grid[i, 0].Value);
            }
            sw.Close();
        }
        public static void FindOddText(string fileName, DataGridView grid)
        {
            double amount_of_odds = 0;
            int i = 0;
            StreamReader sr = new StreamReader(fileName);
            while (!sr.EndOfStream)
            {
                if(grid.ColumnCount == i)
                {
                    grid.ColumnCount++;
                }
                int temp = Convert.ToInt32(sr.ReadLine());
                grid[i, 0].Value = temp;
                if (temp % 2 == 0)
                {
                    amount_of_odds++;
                }
                i++;
            }
            sr.Close();

            MessageBox.Show($"Кількість парних чисел {amount_of_odds}");
        }
        public static void CreateDataFile(string fileName, DataGridView grid)
        {
            FileStream fs = new FileStream(fileName, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            for (int i = 0; i < grid.ColumnCount; i++)
            {
                bw.Write(Convert.ToInt32(grid[i, 0].Value));
            }
            bw.Close();
            fs.Close();
        }
        public static void FindOddData(string fileName, DataGridView grid)
        {
            int amount_of_odds = 0;
            FileStream fs = new FileStream(fileName, FileMode.Open);
            BinaryReader br = new BinaryReader(fs);
            while (fs.Position < fs.Length)
            {
                    if (br.ReadInt32()%2 == 0)
                {
                    amount_of_odds++;
                }
            }
            br.Close();
            fs.Close();

            MessageBox.Show($"Кількість парних чисел {amount_of_odds}");
        }

    }
}
